using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesExercise_20_1_21
{
    class Program
    {
        

        // The following method should return true if each element in the squares sequence
        // is equal to the square of the corresponding element in the numbers sequence.
        // Try to write the entire method using only LINQ method calls, and without writing
        // any loops.
        public static bool TestForSquares(IEnumerable<int> numbers, IEnumerable<int> squares)
        {
            IEnumerable<int> powNumbers = numbers.Select(_ => (int)Math.Pow(_, 2)).ToList();
            string lines = string.Join(Environment.NewLine, numbers);
            return string.Join(Environment.NewLine, squares).SequenceEqual(string.Join(Environment.NewLine, powNumbers));
        }
        //Given a sequence of words, get rid of any that don't have the character 'e' in them,
        // then sort the remaining words alphabetically, then return the following phrase using
        // only the final word in the resulting sequence:
        //    -> "The last word is <word>"
        // If there are no words with the character 'e' in them, then return null.
        //
        // TRY to do it all using only LINQ statements. No loops or if statements.
        public static string GetTheLastWord(IEnumerable<string> words)
        {
            string searchLetter= "e";
            IEnumerable<string> resultList = words.Where(q => searchLetter.ToCharArray().All(p => q.Contains(p)));
            return $"The last word is <{resultList.LastOrDefault()}>";
        }


        static void Main(string[] args)
        {
            List<int> list = new List<int>() { 1, -2, 3, -4, 6, -100, 2, 3 };

            
            List<string> strings = new List<string>() { "play","eat","Switch","app" };
            List<string> strings2 = new List<string>() { "play", "app" };
            Console.WriteLine(GetTheLastWord(strings2));
            Console.WriteLine(GetTheLastWord(strings));
            

            List<int> list2 = new List<int>() { 1, -2, 3 };
            List<int> list3 = new List<int>() { 1, 4, 9 };
            Console.WriteLine(TestForSquares(list2, list3));
            Console.WriteLine(TestForSquares(list, list3));
        }
    }
}
